
/* global AFRAME */
if (typeof AFRAME === 'undefined') {
    throw new Error('Component attempted to register before AFRAME was available.');
}

/*
 * parseNKP
 * Parse a NetGUI NK file, producing a topo object, with
 * nodes and connections
 */
function parseNKP (content) {

    function getQuoted (text, from=0) {
        const start = text.indexOf('"', from);
        const end = text.indexOf('"', start+1);
        quoted = text.slice(start+1, end);
        return quoted;            
    };

    function parseNodes (content) {
        let nodes = [];
        for (line of content.split('\n')) {
            if (line.length < 10) {
                continue;
            } 
            let node = {}
            const coords_start = line.indexOf('position(');
            const coords_end = line.indexOf(')');
            const coords_str = line.slice(coords_start+9, coords_end);
            const coords = coords_str.split(',');
            node.x = Number(coords[0]);
            node.z = Number(coords[1]);
            const equip_start = line.indexOf('; ', coords_end+1);
            const equip_end = line.indexOf('(', coords_end+1);
            node.equipment = line.slice(equip_start+2, equip_end);
            node.name = getQuoted(line, equip_end+1);
            nodes.push(node);
        }
        return nodes
    };
    
    function parseConns (content) {
        let conns = [];
        let in_line = false;
        let conn;
        for (line of content.split('\n')) {
            if (line.length < 2) {
                continue;
            }
            if (line == "<link>") {
                in_line = true;
                conn = {};
            } else if (line == "<\\link>") {
                in_line = false;
                conns.push(conn);
            } else if (in_line) {
                if (line.indexOf('Connect(') != -1) {
                    conn.from = getQuoted(line);
                } else if (line.indexOf('To(') != -1) {
                    conn.to = getQuoted(line);
                };
            }
        }
        return conns
    };

    let topo = {};
    const nodes_start = content.indexOf('<nodes>');
    const nodes_end = content.indexOf('<\\nodes>');
    if (nodes_start == -1 || nodes_end == -1)  {
        throw new Error("NKP malformed, <nodes> section");
    } else {
        let nodes_content = content.slice(nodes_start+7, nodes_end);
        topo.nodes = parseNodes(nodes_content)
    };
    const conns_start = content.indexOf('<connections>');
    const conns_end = content.indexOf('<\\connections>');
    if (conns_start == -1 || conns_end == -1) {
        throw new Error("NKP malformed, <connections> section");
    } else {
        let conns_content = content.slice(conns_start+13, conns_end);
        topo.conns = parseConns(conns_content)
    };
    return topo
};

AFRAME.registerComponent('netvis-network', {
    schema: {
        topo_url: {type: 'string', default: 'netgui.nkp'},
        names_url: {type: 'string', default: 'hosts.json'},
        pkts_url: {type: 'string', default: 'packets.json'},
        height: {type: 'number', default: 0},
        connectionscolor: {type: 'string', default: 'red'},
        run: {type: 'boolean', default: false},
        // Fix geometry by centering topology in (0,0).
        fix_geometry: {type: 'boolean', default: true}
    },

    init: function() {
    	finalConnectionsLinks=[]

        const data = this.data;
        const this_el = this.el;
        const self = this;

        // topo.conns -> conns
        function annotateLinks(conns, nodes, srcnodes) {
            // Times a node appeared in "from" or "to" in conns
            let times = {};
            for (const conn of conns) {
                let from_node = nodes[conn.from];
                let to_node = nodes[conn.to];
                let from_name = from_node.name;
                let to_name = to_node.name;
                times[from_name] = (times[from_name] || 0) + 1;
                times[to_name] = (times[to_name] || 0) + 1;
                let src_addr = from_node.ifaces[times[from_name]-1].hwaddr;
                if (to_node.ifaces.length != 0) {
                    // To_node is not a hub, fill in path and reverse path
                    let dest_addr = to_node.ifaces[times[to_name]-1].hwaddr;
                    let path = {src_addr: src_addr, src_name: from_name,
                        link: conn, dest_node: to_node};
                    from_node.paths[dest_addr] = path;
                    // Now, reverse path
                    let path2 = {src_addr: dest_addr, src_name: to_name,
                        link: conn, dest_node: from_node};
                    to_node.paths[src_addr] = path2;
                } else {
                    // To_node is a hub, fill in only reverse path
                    let path = {src_addr: null, src_name: to_name,
                        link: conn, dest_node: from_node};
                    to_node.paths[src_addr] = path;
                }
            };
            // Now, for all nodes connected to hubs, fill in the direct path
            // For all of those nodes, create a path to all other nodes
            // connected to the hub
            for (const hub of Object.values(nodes)) {
                if (hub.ifaces.length == 0) {
                    // This is a hub
                    let addrs = Object.keys(hub.paths);
                    for (const addr of addrs) {
                        // addr: address of the node in which to add paths
                        let link = hub.paths[addr].link;
                        let node = srcnodes[addr];
                        let node_name = node.name;
                        for (const dest_addr of addrs) {
                            // destaddr: address of the dest node (beyond the hub)
                            if (dest_addr == addr) { continue; };
                            let path = {src_addr: addr, src_name: node_name,
                                link: link, dest_node: hub};
                            node.paths[dest_addr] = path;
                        }
                    };
                }
            }
            return nodes;
        };

        /*
         * buildNodes. Build the nodes object: properties are node names,
         * values are nodes (with their name, equipment, position, ifaces,
         * routing table, etc). 
         */
        function buildNodes(topo, names) {
            let nodes = {}
            for (topo_node of topo.nodes) {
                let node = structuredClone(topo_node);
                let name = node.name;
                node.ifaces = [];
                // Paths will be filled in later, by annotateLinks
                node.paths = {};
                for (iface of names.interfaces) {
                    if (name in iface) {
                        node.ifaces.push(iface[name]);
                    };
                };
                if (names.nodes_info.hasOwnProperty(name) &&
                    names.nodes_info[name].hasOwnProperty('routing_table')) {
                    node.table =
                        structuredClone(names.nodes_info[name]['routing_table']);
                } else {
                    node.table = [];
                };
                nodes[name] = node;
            };
            return nodes;
        };

        /*
         * buildSrcNodes. Build the srcnodes object: properties are
         * iface addrs (which could be src addrs), values are nodes
         */
        function buildSrcNodes(nodes) {
            srcnodes = {};
            for (node of Object.values(nodes)) {
                console.log("network init (node):", node)
                for (iface of node.ifaces) {
                    srcnodes[iface['hwaddr']] = node;
                } 
            };
            return srcnodes;
        };

        /*
         * Fix positions of nodes so that they are centered around "0 0 0"
         * Usually node positions, as defined in NetGUI, are not centered.
         */
        function fix_geometry(nodes) {
            lnodes = Object.values(nodes);
            let range = {x: {max: lnodes[0].x, min: lnodes[0].x},
                z: {max: lnodes[0].z, min: lnodes[0].z}};
            console.log("Range (init):", range, lnodes);
            for (node of lnodes.slice(1)) {
                console.log("Computing range:", node.x, node.z, node);
                for (dim of ['x', 'z']) {
                    console.log("Comparing range:", node[dim], range[dim].max, range[dim].min)
                    if (node[dim] > range[dim].max) {
                        range[dim].max = node[dim];
                    } else if (node[dim] < range[dim].min) {
                        range[dim].min = node[dim];
                    };
                };
            };
            x_desp = (range.x.max + range.x.min) / 2.0;
            z_desp = (range.z.max + range.z.min) / 2.0;
            console.log("Range:", x_desp, z_desp, range);
            for (node of lnodes) {
                node.x = node.x - x_desp;
                node.z = node.z - z_desp;
            }
            return nodes;
        };

        this.fetchDocs(data.topo_url, data.names_url, data.pkts_url).then(
            ([topo, names, pkts]) => {
                self.pkts = pkts;
                self.nodes = buildNodes(topo, names);
                if (data.fix_geometry) {
                    self.nodes = fix_geometry(self.nodes);
                };
                self.create(topo, self.nodes);
                self.srcnodes = buildSrcNodes(self.nodes);
                self.nodes = annotateLinks(topo.conns, self.nodes, self.srcnodes);
                console.log("network init (nodes, pkts, topo, srcnodes):",
                    self.nodes, pkts, topo, self.srcnodes);
                self.running_pkt = 0;
                console.log("about to emit event for update")
                this_el.emit("netvis-network-initialized");             
            });

        // Create div element (outside a-scene) for building HTML texts
        // used by banners (netvis-infopanel component)
        let templates = document.createElement("div");
        templates.setAttribute('id', 'netvis-templates');
        document.body.appendChild(templates);
    },

    update: function (old_data) {

        let this_el = this.el;
        let data = this.data;
        let self = this;

        /*
        * runPkt
        * Make the next packet run through the network
        */
        function runPkt () {

            if (self.running_pkt >= self.pkts.length) {
                console.log('runPkt: No more pkts, nothing to do');
                return;
            };

            if (!data.run) {
                console.log('runPkt: data.run is false, nothing to do');
                return;
            };

            function visPkt(pkt, this_el, nodes_path, scale, height) {

                function scene_pos(node, scale, height) {
                    return {x: node.x * scale, y: height, z: node.z * scale}
                };

                let pkt_el = document.createElement('a-entity');
                pkt_el.setAttribute('netvis-packet', {opacity: 1});
                pkt_el.setAttribute('id', `packet-${pkt.i}`);
                pkt_el.setAttribute('position',
                    scene_pos(nodes_path[0], scale, height+0.2));
                let path = [scene_pos(nodes_path[0], scale, height+0.8)];
                for (node of nodes_path.slice(1,-1)) {
                    path.push(scene_pos(node, scale, height+0.8))
                    path.push(scene_pos(node, scale, height+0.2))
                    path.push(scene_pos(node, scale, height+0.8))
                };
                path.push(scene_pos(nodes_path.at(-1), scale, height+0.8))
                path.push(scene_pos(nodes_path.at(-1), scale, height+0.2))
                console.log("visPkt (path):", path, nodes_path, nodes_path.slice(1,-1));
                pkt_el.setAttribute('netvis-moving', {path: path, 
                    end_dur: 2000,
                    dissolve_prop: 'netvis-packet.opacity'});
                this_el.appendChild(pkt_el);
                console.log('Packet added:', pkt.id, this_el, pkt_el, path, height);
                return pkt_el;
            };

            function bcast_pkts (event) {
                console.log("bcast:", event);
                if (event.detail >= 3) {
                    pkt_el.removeEventListener('netvis-pathcomplete', bcast_pkts);
                    for (path of bcast_paths) {
                        let bcast_el = visPkt(pkt, this_el, path,
                            self.scale, data.height);
                    };
                };
            };

            let pkt = self.pkts[self.running_pkt]
            pkt.i = self.running_pkt;
            console.log("runPkt:", self.running_pkt, pkt);
            let nodes_path = [];
            let pkt_el;
            let bcast_paths = [];
            if (pkt.dst != "ff:ff:ff:ff:ff:ff") {
                let src_node = self.srcnodes[pkt.src];
                nodes_path.push(src_node);
                let path = src_node.paths[pkt.dst];
                console.log("runPkt (path, dest_node):", path, path.dest_node);
                let dest_node = path.dest_node;
                nodes_path.push(dest_node);
                console.log("runPkts (src_node, path):", src_node, path);
                if (dest_node.ifaces.length == 0) {
                    // This is a hub, let the packet go on
                    let path = dest_node.paths[pkt.dst];
                    let then_node = path.dest_node;
                    nodes_path.push(then_node);
                };
                pkt_el = visPkt(pkt, this_el, nodes_path,
                    self.scale, data.height);
            } else {
                let src_node = self.srcnodes[pkt.src];
                nodes_path.push(src_node);
                let dest_node;
                for (path_data of Object.values(src_node.paths)) {
                    if (path_data.src_addr == pkt.src) {
                        dest_node = path_data.dest_node;
                        break;
                    };
                };
                nodes_path.push(dest_node);
                if (dest_node.ifaces.length == 0) {
                    // This is a hub, let the packet go on
                    // Packet will follow though first path, new packets for the rest
                    let paths = []
                    for (path of Object.values(dest_node.paths)) {
                        if (path.dest_node != dest_node) {
                            paths.push(path);
                        }
                    };
                    let then_node = paths[0].dest_node;
                    nodes_path.push(then_node);
                    for (path of paths.slice(1)) {
                        let bcast_path = [dest_node, path.dest_node];
                        bcast_paths.push(bcast_path);
                    }
                };
                pkt_el = visPkt(pkt, this_el, nodes_path,
                    self.scale, data.height);
            };
            self.running_pkt ++;
            if (pkt_el) {
                pkt_el.addEventListener('netvis-done', runPkt);
                if (bcast_paths.length > 0) {
                    pkt_el.addEventListener('netvis-pathcomplete', bcast_pkts);
                };    
            } else {
                // If for some reason there was no packet, just wait for a while
                setTimeout(runPkt, 2000);
            };
        };

        console.log("update starting")
        if (data.run) {
            console.log("update data.run is true")
            if (this.hasOwnProperty('running_pkt')) {
                console.log("update running_pkt is defined");
                runPkt();
            } else {
                console.log("update running_pkt is not defined");
                this_el.addEventListener("netvis-network-initialized", function() {
                    console.log("update init event received");
                    runPkt();
                });
            };
        };
    },

    /*
        fetchDocs

        Fetch documents needed to produce a network component
        (topology, host names and packets).
        Returns a promise.
    */
    fetchDocs: async function(topo_url, names_url, pkts_url) {
        const [topo_resp, names_resp, pkts_resp] = await Promise.all([
            fetch(topo_url),
            fetch(names_url),
            fetch(pkts_url)
        ]);
        let topo, names, pkts;
        if (topo_resp.status == 200) {
            topo_text = await topo_resp.text();
            topo = parseNKP(topo_text);
        } else {
            throw new Error("Error fetching" + topo_url + topo_resp.toString());
        }
        if (names_resp.status == 200) {
            names = await names_resp.json();
        } else {
            throw new Error("Error fetching" + names_url + names_resp.toString());
        }
        if (pkts_resp.status == 200) {
            pkts = await pkts_resp.json();
        } else {
            throw new Error("Error fetching" + pkts_url + pkts_resp.toString());
        }
        return [topo, names, pkts];
    },

    /*
     *  create
     *  Create all the elements of the network as a hierarchy under
     *  the network element.
     */
    create: function (topo, nodes) {

        const data = this.data;

        /*
        * createNodes
        * Create DOM elements for all nodes
        */
        function createNodes (el, nodes, models, scale, height) {

            for (const node of Object.values(nodes)) {
                // find the GLTF model for this node
                let model = models[node.equipment];

                let info = '<table style="border-spacing: 1rem; text-align: center">' +
                    '<tr><th>Destination</th>' +
                    '<th>Mask</th>' +
                    '<th> Gateway</th>' +
                    '<th>Iface</th>' + '</tr>'

                for (const entry of node.table){
                    let entry_txt = '<tr>' +
                        `<td>${entry[0]}</td>` +
                        `<td>${entry[1]}</td>` +
                        `<td>${entry[2]}</td>` +
                        `<td>${entry[3]}</td>` + '</tr>';
                    info += entry_txt;
                }
                info += "</table>"

                
                let element = document.createElement('a-entity');
                element.setAttribute('netvis-node', {
                    name: node.name,
                    model: model.name,
                    scale: model.scale,
                    info: info
                });
                element.setAttribute('position', {
                    x: node.x * scale,
                    y: height,
                    z: node.z * scale
                });
                element.setAttribute('id', 'node-' + node.name);

                el.appendChild(element);
                }
        };

        /*
         * function createLinks
         * Create DOM elements for all links
         */
        function createLinks(this_el, topo, nodes, scale, height) {

            for (let link of topo.conns) {
                from_node = nodes[link.from];
                to_node = nodes[link.to];
                let link_el = document.createElement('a-entity');
                link_el.setAttribute('netvis-link', {
                    from: {x: from_node.x * scale,
                        y: height,
                        z: from_node.z * scale},
                    to: {x: to_node.x * scale,
                        y: height,
                        z: to_node.z * scale},
                    color: data.connectionscolor
                });
                this_el.appendChild(link_el);
            };
        };

        // Build all node elements in the DOM
        createNodes(this.el, nodes, this.models, this.scale, data.height);
        // Build all link elements in the DOM
        createLinks(this.el, topo, nodes, this.scale, data.height);
    },


    /*
     * GLTF models for give prefixes
     */
    models: {
        'NKCompaq': {name: '#pc', scale: 1.0/500},
        'NKHub': {name: '#hub', scale: 1.0/2},
        'NKRouter': {name: '#router', scale: 1.0/500},
        'NKDNS': {name: '#pc', scale: 1.0/500},
    },

    /*
     * Scale (multiply location in topology document by this)
     */
    scale: 1.0/100,
    
});


/*
 * netvis-node component
 * Shows a node in the network.
 * When clicked, pops up a banner with some info
 */
AFRAME.registerComponent('netvis-node', {

    schema: {
        name: {type: 'string', default: '#pc'},
        model: {type: 'string', default: '#pc'},
        scale: {type: 'number', default: 1.0/100},
        info: {type: 'string', default: "Info"}
    },

    init: function () {

        const data = this.data;
        const this_el = this.el;

        console.log("netvis-node init()");
        function infoListener () {
            let old_banner = this_el.querySelector('.netvis-node-banner');
            if (old_banner) {
                // Remove banner
                this_el.removeChild(old_banner);
            } else {
                // Pop up banner
                let banner = document.createElement('a-entity');
                banner.setAttribute('netvis-infopanel', {
                    name: `${data.name}-banner`, text: data.info});
                banner.setAttribute('position', {x: 0, y: 1, z: 0});
                banner.setAttribute('class', 'netvis-node-banner');
                this_el.appendChild(banner);
            };
        };
    
        let model = document.createElement('a-entity');
        model.setAttribute('gltf-model', data.model);
        model.setAttribute('scale', {
            x: data.scale, y: data.scale, z: data.scale
        });
        model.setAttribute('rotation', '0 -90 0');
        console.log(this.el, model, data.model);
        this.el.addEventListener('click', infoListener);
        this.el.appendChild(model);
        let name = document.createElement('a-entity');
        name.setAttribute('netvis-infopanel', {
            name: `${data.name}-name`, text: data.name});
        name.setAttribute('position', {x: 0, y: 1, z: 0});
        name.setAttribute('class', 'netvis-node-name');
        this.el.appendChild(name);
    },

});

/*
 * netvis-link component
 * Shows a link in the network.
 */
AFRAME.registerComponent('netvis-link', {

    schema: {
        from: {type: 'vec3'},
        to: {type: 'vec3'},
        color: {type: 'string'},
    },

    init: function () {
        const data = this.data;

        let tube = document.createElement('a-tube');
        tube.setAttribute('path', `${data.from.x} ${data.from.y} ${data.from.z}` +
            `, ${data.to.x} ${data.to.y} ${data.to.z}`);
        tube.setAttribute('radius', 0.05);
        tube.setAttribute('material', {color: data.color});
        this.el.appendChild(tube);
    }
});

AFRAME.registerComponent('netvis-infopanel', {

    schema: {
        name: {type: 'string', default: 'banner'},
        text: {type: 'string', default: 'Info'},
    },

    init: function () {
        const data = this.data;

        console.log("netvis-infopanel init");

        let info_template = document.createElement('section');
        info_template.innerHTML = data.text;
        info_template.style = "display: inline-block; background: white;" +
            " color: black; border-radius: 1em; padding: 1em; margin:0;";
        info_template.id = 'netvis-template-node-' + data.name;
        let templates = document.getElementById('netvis-templates');
        templates.appendChild(info_template);

        let info = document.createElement('a-entity');
        info.setAttribute('html', {html: "#" + info_template.id});
        info.setAttribute('scale', {x: 5, y: 5, z: 5})
        info.setAttribute('look-at', '[camera]');
        this.el.appendChild(info);
    }
});

/*
 * Static packet
 * Just a packet, with all its protocolos, info banners, etc.
 */
AFRAME.registerComponent('netvis-packet', {

    schema: {
        name: {type: 'string', default: ''},
        protos: {type: 'array', default: ['Eth', 'IP', 'TCP', 'HTTP']},
        protos_info: {type: 'array', default: ['eth', 'ip', 'tcp', 'http']},
        proto_height: {type: 'number', default: 0.3},
        radius: {type: 'number', default: 0.2},
        opacity: {type: 'number', default: 1}
    },

    init: function () {
        const data = this.data;
        const colors = this.colors;
        const this_el = this.el;

        console.log("netvis-packet init:", data.protos, data.protos_info);
        function createCyl(layer, proto){
            position = {x: 0, y: layer * data.proto_height, z: 0};
            let layer_el = document.createElement('a-entity');
            layer_el.setAttribute('geometry', {
                primitive: 'cylinder',
                height: data.proto_height,
                radius: data.radius});
            layer_el.setAttribute('material', {color: colors[proto]});
            layer_el.setAttribute('position', position);
            this_el.appendChild(layer_el);    
        };

        function computeInfo(protos, protos_info) {
            let text = '<table style="border-spacing: 1rem; text-align: center">'
            for (const [i, proto] of protos.entries()){
                let proto_txt = '<tr>' +
                    `<td>${proto}</td>` +
                    `<td>${protos_info[i]}</td>` +
                    '</tr>';
                text += proto_txt;
            };
            text += "</table>";
            return text;
        }

        for (const [layer, proto] of data.protos.entries()) {
            createCyl(layer, proto);
        };
        let banner = document.createElement('a-entity');
        let banner_height = data.proto_height * (data.protos.length / 2) + 1;
        let banner_info = computeInfo(data.protos, data.protos_info);
        banner.setAttribute('netvis-infopanel', {
            name: `${data.name}-banner`, text: banner_info});
        banner.setAttribute('position', {x: 0, y: banner_height, z: 0});
        banner.setAttribute('class', 'netvis-node-banner');
        this_el.appendChild(banner);
    },

    update: function (old_data) {
        let data = this.data;

        if (old_data && (old_data.opacity != data.opacity)) {
            for (const layer_el of this.el.children) {
                layer_el.setAttribute('material', {
                    opacity: data.opacity});
            };
        };
    },

    colors: {
        'Eth': 'red',
        'IP': 'blue',
        'TCP': 'green',
        'HTTP': 'yellow'
    }
});

/*
 * Moving component
 * Makes its element wait for "start_dur" then move following path,
 * taking the corresponding time in moving_dur to reach there, then sit
 * there for "end_dur", and finally removes the element from DOM
 * (maybe with a dissolve effect if specified).
 */
AFRAME.registerComponent('netvis-moving', {

    schema: {
        // Path to follow from position: array of vec3 
        path: {type: 'array'},
        // Time to stay before start moving (ms)
        start_dur: {type: 'number', default: 1000}, 
        // Duration for each step in path (ms)
        moving_dur: {type: 'array'},
        // Time to stay after moving (time finishing) (ms)
        end_dur: {type: 'number', default: 1000},
        // Finish by destroying the element
        destroy: {type: 'boolean', default: true},
        // Dissolve effect while finishing
        dissolve: {type: 'boolean', default: true},
        // Property to use for dissolve effect
        dissolve_prop: {type: 'string', default: 'material.opacity'}
    },

    init: function () {
        const this_el = this.el;
        const data = this.data;
        if (!data.hasOwnProperty('moving_dur')) {
            data.moving_dur = Array(path.lenght).fill(1000);
        };
        let path_step = 0;

        function move (dur) {
            this_el.setAttribute('animation', {
                property: 'position', dur: dur, to: data.path[path_step]});
            path_step ++;
        };

        function dissolve (dur) {
            finishing = true;
            console.log('moving (dissolve):', data.dissolve_prop);
            try {
                this_el.setAttribute('animation', {
                    property: data.dissolve_prop, dur: dur, to: 0}, true);
            } catch (e) {
                // Likely dissolve_prop does not exist
                console.log("netvis-moving: Exception raised. " +
                    `Likely, dissolve_prop (${data.dissolve_prop}) ` +
                    "does not exist", e);
                // Emit event so that the element can finish
                this_el.emit("animationcomplete");
            }
        };

        function remove () {
            this_el.emit('netvis-done');
            console.log('moving (remove):', data.destroy);
            if (data.destroy) {
                this_el.parentNode.removeChild(this_el);
            };
        };

        let finishing = false;
        // Prepare removal (end_dur since animation is complete)
        this_el.addEventListener("animationcomplete", function() {
            console.log("moving:", path_step, data.path);
            if (path_step < data.path.length) {
                this_el.emit('netvis-pathcomplete', path_step);
                move(data.moving_dur[path_step]);
            } else {
                console.log("moving (finishing):", finishing);
                if ( data.dissolve ) {
                    if ( !finishing ) {
                        dissolve(data.end_dur);
                    } else {
                        remove();
                    };
                } else {
                    setTimeout(remove, data.end_dur);
                };
            };
        });
        // Prepare animation (start_dur since now)
        setTimeout(move, data.start_dur);
    }
});

/*
 * Controller component
 * Controls an element (usually with a network component)
 * by changing some of its properties when receiving events.
 * Currently used to toggle 'run' property which makes network
 * run or stop.
 */
AFRAME.registerComponent('netvis-controller', {

    schema: {
        // Event to listn to
        event: {type: 'string', default: 'click'},
        // Id of the controlled element (should have the network component to control)
        controlled: {type: 'string', default: 'network'}
    },

    init: function () {
        let data = this.data;
        let this_el = this.el;
        let toggle = false;

        this_el.addEventListener(data.event, function () {
            console.log("Click, should trigger message in network update")
            network_el = document.getElementById(data.controlled);
            console.log("Click, should trigger message in network update", network_el)
            toggle = !toggle;
            network_el.setAttribute('netvis-network', {run: toggle});
        });
    }

});
